#include "unity.h"
#include "logic.h"
#include "universe.h"
#include "graphics.h"

void test_create_uni(){
    int width = -1;
    int height = 100;
    TEST_ASSERT_NULL(create_uni(&width, &height));
    width = 100;
    height = -1;
    TEST_ASSERT_NULL(create_uni(&width, &height));
    width = 100;
    height = 100;
    TEST_ASSERT_NOT_NULL(create_uni(&width, &height));
}

void test_destroyUniverse(){
    int width = 100;
    int height = 100;
    int** uni = create_uni(&width, &height);
    TEST_ASSERT_EQUAL_HEX8(1, destroy_uni(uni, 100));
    uni = create_uni(&width, &height);
    TEST_ASSERT_EQUAL_HEX8(-99, destroy_uni(uni, -1));
}

void test_width_height(){
    int width = 100;
    int height = 100;
    int** uni = create_uni(&width, &height);
    TEST_ASSERT_EQUAL_HEX8(-99, cell_life(uni, 1, 50));
    TEST_ASSERT_EQUAL_HEX8(-98, cell_life(uni, 40, 1));
    TEST_ASSERT_EQUAL_HEX8(-99, cell_life(uni, 1, 1));
    TEST_ASSERT_EQUAL_HEX8(0, cell_life(uni, 100, 100));
    TEST_ASSERT_EQUAL_HEX8(0, cell_life(uni, 3, 3));
}
void test_drawCells(){
    int width = 100;
    int height = 100;
    int** uni = create_uni(&width, &height);
    int cell_size = 5;
    SDL_Window* window = SDL_CreateWindow("GoL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                          0, 0, SDL_WINDOW_SHOWN);
    SDL_Surface* screenSurface = SDL_GetWindowSurface(window);

    random_universe(uni, width, height, 2);
    TEST_ASSERT_EQUAL_HEX8(-99, drawCells(uni, 1, 100, cell_size, screenSurface, window));
    TEST_ASSERT_EQUAL_HEX8(-98, drawCells(uni, 100, 1, cell_size, screenSurface, window));
    TEST_ASSERT_EQUAL_HEX8(1, drawCells(uni, 100, 100, cell_size, screenSurface, window));

    random_universe(uni, width, height, 9);
    TEST_ASSERT_EQUAL_HEX8(-1, drawCells(uni, 100, 100, cell_size, screenSurface, window));
    TEST_ASSERT_EQUAL_HEX8(-99, drawCells(uni, 1, 100, cell_size, screenSurface, window));
    TEST_ASSERT_EQUAL_HEX8(-98, drawCells(uni, 100, 1, cell_size, screenSurface, window));
}

void test_resetUniverse(){
    int width = 100;
    int height = 100;
    int** uni = create_uni(&width, &height);

    TEST_ASSERT_EQUAL_HEX8(1, reset_universe(uni, 100, 100));
    TEST_ASSERT_EQUAL_HEX8(1, reset_universe(uni, 1, 100));
    TEST_ASSERT_EQUAL_HEX8(1, reset_universe(uni, 100, 1));
    TEST_ASSERT_EQUAL_HEX8(-99, reset_universe(uni, -1, 100));
    TEST_ASSERT_EQUAL_HEX8(-98, reset_universe(uni, 100, -1));
}

void test_load_uni(){
    int width = 100;
    int height = 100;
    int iterations = 100;
    int** uni = create_uni(&width, &height);
    save_uni(uni, width, height, iterations);
    TEST_ASSERT_EQUAL_HEX8(1,load_uni(uni, &width, &height, &iterations));
    width = 1;
    save_uni(uni, width, height, iterations);
    TEST_ASSERT_EQUAL_HEX8(-99,load_uni(uni, &width, &height, &iterations));
    width = 100;
    height = 1;
    save_uni(uni, width, height, iterations);
    TEST_ASSERT_EQUAL_HEX8(-98,load_uni(uni, &width, &height, &iterations));
    height = 100;
    iterations = -1;
    save_uni(uni, width, height, iterations);
    TEST_ASSERT_EQUAL_HEX8(-97,load_uni(uni, &width, &height, &iterations));
}
int main(){
    UNITY_BEGIN();
    RUN_TEST(test_create_uni);
    RUN_TEST(test_width_height);
    RUN_TEST(test_drawCells);
    RUN_TEST(test_resetUniverse);
    RUN_TEST(test_destroyUniverse);
    RUN_TEST(test_load_uni);
    return UNITY_END();
}