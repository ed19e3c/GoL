#ifndef GOL_LOGIC_H
#define GOL_LOGIC_H

#endif //GOL_LOGIC_H
int reset_universe(int** uni, int width, int height);
int cell_life(int** uni, int width, int height);
int random_universe(int** uni, int width, int height, int mod);
int** create_uni(int *width, int *height);
void show_universe(int** uni, int width, int height);
int destroy_uni(int** uni, int width);
int** realloc_uni(int** uni, int *width, int *height);
