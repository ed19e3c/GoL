cmake_minimum_required(VERSION 2.8.12.2)
project(GoL)
set(CMAKE_C_STANDARD 11)

find_package(SDL2 REQUIRED)
include_directories(GoL ${SDL2_INCLUDE_DIRS})
include_directories(GoL_tests ${SDL2_INCLUDE_DIRS})
include_directories(Unity/include)

add_executable(GoL
        logic.c
        logic.h
        graphics.c
        graphics.h
        universe.c
        universe.h
        main.c)
add_executable(GoL_tests testFile.c logic.c graphics.c universe.c Unity/src/unity.c)
target_link_libraries(GoL ${SDL2_LIBRARIES})
target_link_libraries(GoL_tests ${SDL2_LIBRARIES})

install(TARGETS GoL RUNTIME DESTINATION bin)