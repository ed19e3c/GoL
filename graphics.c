#include "graphics.h"
#include "logic.h"
#include "universe.h"
int drawCells(int **uni, int w, int h, int cs, SDL_Surface *surf, SDL_Window *window){
    Uint32 white = SDL_MapRGB(surf->format, 0xFF, 0xFF, 0xFF);
    Uint32 black = SDL_MapRGB(surf->format, 0x00, 0x00, 0x00);
    if(w < 2){ //If width < 0, size of universe becomes negative, which is invalid.
        printf("Error: width is less than 0!");
        return -99;
    }
    else if(h < 2){ //If height < 0, size of universe becomes negative, which is invalid.
        printf("Error: height is less than 0!");
        return -98;
    }
    for (int i = 0; i < w-2 ; i+= 1){
        for(int j = 0; j < h-2 ; j += 1){
            SDL_Rect rect;
            rect.x=i * cs;
            rect.y=j * cs;
            rect.w=cs;
            rect.h=cs;
            // for every cell in the universe, a square is drawn.
            if(uni[i][j] == 1){
                SDL_FillRect(surf, &rect, white);
                // if the cell has a one, a white square is drawn.
            }
            else if(uni[i][j] == 0){
                SDL_FillRect(surf, &rect, black);
                // if the cell has a zero, a black square is drawn.
            }
            else{
                // universe stores incorrect values, so the program is terminated.
                return -1;
            }
        }
    }
    SDL_UpdateWindowSurface(window);
    // updates the surface to show the evolution of cells in the window.
    return 1;
}

int graphics(int width, int height, int iterations){
    int close = 0;
    int iteration = iterations;
    int iteration_current = 0;
    int cell_size = 5;
    int uni_width = width;
    int uni_height = height;
    int** uni;
    if(uni_width < 2){ //If width < 0, size of universe becomes negative, which is invalid.
        printf("Error: width is less than 0!");
        return -99;
    }
    else if(uni_height < 2){ //If height < 0, size of universe becomes negative, which is invalid.
        printf("Error: height is less than 0!");
        return -98;
    }
    else if(iteration < 0){ //If iterations < 0, cannot have negative iterations taken place.
        printf("Error: iterations is less than 0!");
        return -97;
    }
    uni = create_uni(&uni_width, &uni_height);
    int ww = (uni_width-2) * cell_size + 1;
    int wh = (uni_height-2) * cell_size + 1;
    // creates a fresh universe, and window width and window height is declared.
    random_universe(uni, uni_width, uni_height, 2);
    // generates a random universe.

    SDL_Window* window = NULL;
    SDL_Surface* screenSurface;
    // declares a window and surface to be drawn on and for the user to interact with
    if(SDL_Init(SDL_INIT_VIDEO) < 0){
        printf("SDL could not be initialized! Error: %s\n", SDL_GetError());
        // error if SDL has issues initializing
    }
    else{
        window = SDL_CreateWindow("GoL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                  ww, wh, SDL_WINDOW_SHOWN);
        // generates a window that is centered on the screen and has a size set on what was declared above.
        if(window == NULL){
            printf("Window could not be created! Error: %s\n", SDL_GetError());
            // error if window could not be declared
        }
        else{
            SDL_UpdateWindowSurface(window);
            // updates the window with the surface used.
        }
    }
    while(!close){
        SDL_Event event;
        while(SDL_PollEvent(&event)){
            // above commands wait for user input to be entered to process.

            screenSurface = SDL_GetWindowSurface(window);
            drawCells(uni, uni_width, uni_height, cell_size, screenSurface, window);
            // grabs the existing surface on the window and overwrites it with the new generation of cells.
            if(event.type == SDL_QUIT){
                close = 1;
                // if the X button is clicked (top right) program is terminated
            }
            else if(event.type == SDL_KEYDOWN){
                switch(event.key.keysym.sym){
                    case SDLK_RIGHT:
                        drawCells(uni, uni_width, uni_height, cell_size, screenSurface, window);
                        cell_life(uni, uni_width, uni_height);
                        SDL_UpdateWindowSurface(window);
                        iteration_current += 1;
                        // if the right arrow key is entered, the next iteration is generated.
                        break;
                    case SDLK_ESCAPE:
                        close = 1;
                        // if the escape key is entered, the universe is terminated
                        break;
                    case SDLK_s:
                        save_uni(uni, uni_width, uni_height, iteration);
                        // if the S key is entered, saves the current state of the universe to the file.
                        break;
                    case SDLK_l:
                        load_uni(uni, &uni_width, &uni_height, &iteration);
                        // when the L key is entered, the current universe is overwritten with the one on the universe file.
                        iteration_current = iteration;
                        ww = (uni_width-2) * cell_size + 1;
                        wh = (uni_height-2) * cell_size + 1;
                        // the window size and the iterations are recalculated to generate new windows.
                        SDL_DestroyWindow(window);
                        window = SDL_CreateWindow("GoL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                                  ww, wh, SDL_WINDOW_SHOWN);
                        // destroys the existing window to create a new window with different size parameters.
                        SDL_SetWindowSize(window, ww, wh);
                        screenSurface = SDL_GetWindowSurface(window);
                        drawCells(uni, uni_width, uni_height, cell_size, screenSurface, window);
                        SDL_UpdateWindowSurface(window);
                        // draws cells on the new surface, and the new surface is applied to the window.
                        break;
                    case SDLK_r:
                        random_universe(uni, uni_width, uni_height, 2);
                        drawCells(uni, uni_width, uni_height, cell_size, screenSurface, window);
                        cell_life(uni, uni_width, uni_height);
                        SDL_UpdateWindowSurface(window);
                        // when the R key is entered, the universe is randomly generated again.
                        break;
                    case SDLK_a:
                        // when the A key is entered, the program runs for the number of iterations initially entered.
                        for(int i = iteration_current; i<iteration; i++){
                            drawCells(uni, uni_width, uni_height, cell_size, screenSurface, window);
                            cell_life(uni, uni_width, uni_height);
                            SDL_UpdateWindowSurface(window);
                            iteration_current++;
                            SDL_Delay(50);
                        }
                        if(iteration_current == iteration) {
                            save_uni(uni, uni_width, uni_height, iteration);
                            SDL_Delay(1000);
                            SDL_DestroyWindow(window);
                            SDL_Quit();
                            return 1;
                            // once the iterations have reached the number of target iterations, universe is saved and is terminated.
                        }
                }
            }
        }
        SDL_Delay(50);
    }
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 1;
    // quits the program when the user is finished with it.
}

int menu(){
    char command[20];
    int width = 102;
    int height = 102;
    int iterations = 100;
    printf("Hello, welcome to Conway's Game of Life! Please read the readme.txt for extra information of the game!\n\n");
    printf("Please enter a command:\n");
    printf("Play - Creates a window with a randomly generated universe with the set width and height (default set to 100 for both)\n");
    printf("Iterations - Sets number of iterations that the program will run for (default is 100)\n");
    printf("Window - Sets the width and height of the universe\n");
    printf("Quit - Quits the program\n");
    while(1){
        scanf("%s", command);
        if (strcmp(command, "Play") == 0){
            graphics(width, height, iterations);
            // when "Play" is entered, the main program is run
            exit(0);
        }
        else if(strcmp(command, "Iterations") == 0){
            printf("Please enter the number of iterations you want the universe to run for:\n");
            scanf("%d", &iterations);
            // when "Iterations" is entered, allows user to enter number of iterations they want to run for.
        }
        else if(strcmp(command, "Window") == 0){
            printf("Please enter the width and height desired with a space inbetween:\n");
            scanf("%d %d", &width, &height);
            width += 2;
            height += 2;
            // when "Window" is entered, allows user to modify the window width and height.
        }
        else if(strcmp(command, "Exit") == 0){
            exit(0);
            // Quits the program.
        }
        else{
            printf("Try again!\n");
        }
        printf("Please enter a command:\n");
        printf("Play - Creates a window with a randomly generated universe with the set width and height (default set to 100 for both)\n");
        printf("Iterations - Sets number of iterations that the program will run for (default is 100)\n");
        printf("Window - Sets the width and height of the universe\n");
        printf("Quit - Quits the program\n");
    }
}
