#include <stdio.h>
#include <stdlib.h>
#include "logic.h"
int load_uni(int** uni, int *width, int *height, int *iterations){
    FILE *fp;
    fp = fopen("universe.bin", "r");
    // opens the universe file in read mode
    if(fp == NULL){
        printf("no file found");
        return -99;
        // if no file found, code is terminated.
    }
    fscanf(fp,"%i %i %i\n", width, height, iterations);
    // reads the first three files as width, height and iterations of universe.
    uni = realloc_uni(uni, width, height);
    // reallocates memory for the new universe to accomodate for different dimensions of universes.
    if(*width < 2){ //If width < 0, size of universe becomes negative, which is invalid.
        printf("Error: width is less than 0!");
        return -99;
    }
    else if(*height < 2){ //If height < 0, size of universe becomes negative, which is invalid.
        printf("Error: height is less than 0!");
        return -98;
    }
    else if(*iterations < 0){ //If iterations < 0, cannot have negative iterations taken place.
        printf("Error: iterations is less than 0!");
        return -97;
    }
    for(int i = 0; i < *width; i++){
        fread(uni[i], sizeof(int), *height, fp);
        // reads the universe array and is stored.
    }
    return 1;
}

int save_uni(int** uni, int width, int height, int iterations) {
    FILE *fp;
    fp = fopen("universe.bin", "w");
    // opens the universe file in write mode
    if (fp == NULL) {
        printf("file no found");
        return -99;
        // if no file found, code is terminated.
    }
    fprintf(fp, "%i %i %i\n", width, height, iterations);
    // writes first three values in file as width, height and iterations.
    for (int i = 0; i < width; i++) {
        fwrite(uni[i], sizeof(int), height, fp);
        // writes the universe array to the file to be read later.
    }
    fclose(fp);
    return 1;
}
