Welcome to coursework 2 - Conway's Game of Life!
To clone this repo, there are three methods you can try in your terminal:
1. use git clone `https://gitlab.com/ed19e3c/GoL.git`. This will clone the direct cwk2 repository on your machine. (Note that this will likely not work because the repo is private, but worth a shot).
2.  - use git clone `https://gitlab.com/comp1921_2021/ed19e3c.git` to clone the main ed19e3c repository.
    - enter the repository (through the use of cd ed19e3c) and identify that all files are present in the cloned repo. You will notice that the cwk2 repo is empty.
    - run lines of code `git submodule init` and `git submodule update` to fetch all the remaining data.
3. Extract the zip folder and follow the instructions below.

From there, enter the `cwk2` repository (`cd cwk2`) and enter the `cmake-build-debug` folder (`cd cmake-build-debug`), which holds the cmake outputs for the program and the test suite for the program.

- TO RUN THE PROGRAM USE: `./GoL`
- TO RUN THE TEST SUITE OF THE PROGRAM USE: `./GoL_tests`

From there, initial instructions and options are provided to the user, such as changing the dimensions of the universe and the number of iterations they want. Base values are 100 for width, height and number of iterations.

After finishing the tinkering of the universe values, you will be greeted with a random universe that will be generated. Below are available keypresses that the program accepts to interact with the universe:
- `RIGHTARROW KEY`: simulates the next generation of cells. Can be pressed many times or held to generate multiple generations of cells in rapid succession.
- `S KEY`: saves the current universe of cells, holding width and height of universe as well as the current iteration it is on.
- `L KEY`: loads the universe stored in the universe.bin file stored within the cmake-build-debug folder. Also allows for loading of universes with different dimensions from the dimesions you might've started with at the beginning.
- `R KEY`: generates a new random universe of that dimension.
- `A KEY`: before entering the program, you are prompted to enter number of iterations you want to simulate. Pressing the A key will execute that number of iterations, freeze the program to show the user the universe, save the universe and terminate the program.
- `ESCAPE KEY`: quits the program

Note that the git commit history is stored in a separate text file in this repository, as well as the functionality video being stored in a separate .mp3 file.
