#include "logic.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int reset_universe(int** uni, int width, int height){
    if(width < 0){ //If width < 0, size of universe becomes negative, which is invalid.
        printf("Error: width is less than 0!");
        return -99;
    }
    else if(height < 0){ //If height < 0, size of universe becomes negative, which is invalid.
        printf("Error: height is less than 0!");
        return -98;
    }
    for(int i=0; i<height; i++){
        for(int j=0; j<width; j++){
            uni[j][i] = 0; // Otherwise, every cell in the universe is set to 0, "resetting" the universe.
        }
    }
    return 1;
}

void show_universe(int** uni, int width, int height){
    for(int i=0; i<height; i++){
        for(int j=0; j<width; j++){
            printf("%i ", uni[j][i]); // Prints all values of the universe, mostly used for testing.
        }
        printf("\n");
    }
}

int random_universe(int** uni, int width, int height, int mod){
    srand(time(NULL)); // Generates a random seed based on the time.
    for(int i=0; i<height; i++){
        for(int j=0; j<width; j++){
            uni[j][i] = rand() % mod;
            // Generates a random number between the 0 and the modulus number (usually will pick between 0 and 1).
        }
    }
    return 1;
}

int** create_uni(int *width, int *height){
    if(*width < 0){ //If width < 0, size of universe becomes negative, which is invalid.
        printf("Error: width is less than 0!");
        return NULL;
    }
    if(*height < 0){ //If height < 0, size of universe becomes negative, which is invalid.
        printf("Error: height is less than 0!");
        return NULL;
    }
    int** x = malloc(*width * sizeof *x);
    // allocates memory for the size of width.
    for(int i=0; i<*width; i++){
        x[i] = malloc(*height * sizeof *x[i]);
        // for every width in the array, another array of size height is also declared.
    }
    return x;
}

int destroy_uni(int** uni, int width){
    if(width < 0){ //If width < 0, size of universe becomes negative, which is invalid.
        printf("Error: width is less than 0!");
        return -99;
    }
    for(int i=0; i<width; i++){
        free(uni[i]);
        // frees all rows in the 2d array.
    }
free(uni); // frees the variable pointer.
    return 1;
}

int** realloc_uni(int** uni, int *width, int *height) {
    int** temp = NULL;
    temp = create_uni(width, height); // creates a new temporary 2d array.
    //destroy_uni(uni, *width);
    for(int i = 0; i < *width; i++){
        uni[i] = temp[i];
        // allocates new empty values to the universe, generating a fresh universe
    }
    return uni;
}
int cell_life(int** uni, int width, int height){
    int neighbours;
    int **y;
    y = malloc(width * sizeof *y);
    // creates a temporary 2d array to store our cells in when figuring out if they are alive or dead.
    if (width < 2){ //If width < 0, size of universe becomes negative, which is invalid.
        printf("Error: width is less than 0!");
        return -99;
    }
    if (height < 2){ //If height < 0, size of universe becomes negative, which is invalid.
        printf("Error: height is less than 0!");
        return -98;
    }
    for(int i=0; i<width; i++){
        y[i] = malloc(height * sizeof *y[i]);
    }
    reset_universe(y, width, height);
    // resets the temporary array, ready to be used.
    for(int i=1; i<height-1; i++){ // i starts at 1 to accomodate "deadzone," introducing borders to game.
        for(int j=1; j<width-1; j++){ // j starts at 1 to accomodate "deadzone," introducing borders to game.
            neighbours = 0;
            neighbours += uni[j==0 ? width-1 : j-1][i==0 ? height-1 : i-1]; //top left cell
            neighbours += uni[j][i==0 ? height-1 : i-1]; // top cell
            neighbours += uni[j==width-1 ? 0 : j+1][i==0 ? height-1 : i-1]; // top right cell
            neighbours += uni[j==0 ? width-1 : j-1][i]; // left cell
            neighbours += uni[j==width-1 ? 0 : j+1][i]; // right cell
            neighbours += uni[j==0 ? width-1 : j-1][i==height-1 ? 0 : i+1]; // bot left cell
            neighbours += uni[j][i==height-1 ? 0 : i+1]; // bot cell
            neighbours += uni[j==width-1 ? 0 : j+1][i==height-1 ? 0 : i+1]; // bot right cell
            if(neighbours == 3){
                y[j][i] = 1; // if 3 neighbours are found, the cell turns alive.
            }
            else if(neighbours == 2){
                if(uni[j][i] == 1) {
                    y[j][i] = 1; // if 2 neighbours are found and cell is alive, it stays alive.
                }
            }
            else {
                y[j][i] = 0; // otherwise cell is dead
            }
        }
    }
    for(int i=0; i<height; i++){
        for(int j=0; j<width; j++) {
            uni[j][i] = y[j][i];
            // temp universe values are copied over to main universe
        }
    }
    destroy_uni(y, width);
    // frees the temporary universe
    return 0;
}
