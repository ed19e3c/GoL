# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/ervans/GoL/graphics.c" "/home/ervans/GoL/cmake-build-debug/CMakeFiles/GoL.dir/graphics.c.o"
  "/home/ervans/GoL/logic.c" "/home/ervans/GoL/cmake-build-debug/CMakeFiles/GoL.dir/logic.c.o"
  "/home/ervans/GoL/main.c" "/home/ervans/GoL/cmake-build-debug/CMakeFiles/GoL.dir/main.c.o"
  "/home/ervans/GoL/universe.c" "/home/ervans/GoL/cmake-build-debug/CMakeFiles/GoL.dir/universe.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../GoL"
  "/usr/include/SDL2"
  "../GoL_tests"
  "../Unity/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
